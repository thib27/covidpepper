import cv2
from cv2 import dnn_superres
#import time

def upscale():
    #t=time.time() 

    #time.sleep(1)
    # Create an SR object
    sr = dnn_superres.DnnSuperResImpl_create()

    # Read image
    image = cv2.imread('./img/image.jpg')

    # Read the desired model
    sr.readModel('./img/FSRCNN-small_x2.pb')

    # Set the desired model and scale to get correct pre- and post-processing
    sr.setModel("fsrcnn", 2)

    # Upscale the image
    result = sr.upsample(image)

    # Save the image
    cv2.imwrite("./img/image.jpg", result)
    #print("----------------> time : " + str(time.time()-t))