# coding: utf-8
import socket
import time
from paramiko import util
import util_pepper
from naoqi import ALProxy
import qi
ClientMultiSocket = socket.socket()
host = 'localhost'
port = 2004


def main():
    #Connection au serveur
    print('Waiting for connection response')
    try:
        ClientMultiSocket.connect((host, port))
    except socket.error as e:
        print(str(e))
   
    print('Connected , entering loop')
    res = ClientMultiSocket.recv(1024)
    print(res.decode())
    ClientMultiSocket.send(str.encode("pepper"))
    print(ClientMultiSocket.recv(1024).decode())

    #Préparation des proxys + accès ftp
    
    
    #Une fois connecté on peut parler au serveur 
    util_pepper.pepper_loop(ClientMultiSocket)
    '''
    test=True
    tts = ALProxy("ALTextToSpeech", "10.224.0.242", 9559)
    tts.setLanguage("French")
    tts.setVolume(0.6)
    behavior_mng_service=ALProxy("ALBehaviorManager", "10.224.0.242", 9559)
    posture_service = ALProxy("ALRobotPosture", "10.224.0.242", 9559)

    while True:
        time.sleep(0.1)
        #on commence toujours par regarder si on a de la donnée sur le serveur(la fonction s'occupe de traiter la réponse )
        util_pepper.checkData(ClientMultiSocket, tts,behavior_mng_service,posture_service)
        if(test):
            test=False
            ClientMultiSocket.send("noPeopleLeft".encode())
            ClientMultiSocket.recv(1024)

    '''
    
       
    

if __name__=='__main__':
    main()